﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwitchToSprite : MonoBehaviour
{
    public Image sprite;
    public List<SkinnedMeshRenderer> skinnedRenderers;
    public List<MeshRenderer> meshRenderers;

    void Start()
    {
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            foreach(var skinnedRenderer in skinnedRenderers)
            {
                skinnedRenderer.enabled = true;
            }
            foreach (var meshRenderer in meshRenderers)
            {
                meshRenderer.enabled = true;
            }
            sprite.enabled = false;
        }
        else
        {
            foreach (var skinnedRenderer in skinnedRenderers)
            {
                skinnedRenderer.enabled = false;
            }
            foreach (var meshRenderer in meshRenderers)
            {
                meshRenderer.enabled = false;
            }
            sprite.enabled = true;
        }
    }
}
