﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using Photon.Realtime;
using Photon.Pun;
using UnityEngine;
using System.IO;

public class CannonBall : MonoBehaviour {

    public float lifeTime;
    public GameObject explosion;
    public float minY = -20f;
    public PhotonView pv;
	
	// Update is called once per frame
	void Update ()
    {
        pv = this.GetComponent<PhotonView>();
        StatusCheck();
    }

    private void StatusCheck()
    {
        lifeTime -= Time.deltaTime;

        if (lifeTime < 0.0f || transform.position.y < minY)
        {
           DestroyCannonBall();
        }
    }

    public void DestroyCannonBall()
    {
        RPC_DestroyCannonBall();
    }

    [PunRPC]
    private void RPC_DestroyCannonBall()
    {
        if(pv != null)
        {
            pv.RPC("RPC_SpawnDestroyEffect", RpcTarget.All);
        }
    }

    [PunRPC]
    private void RPC_SpawnDestroyEffect()
    {
        //if(PhotonNetwork.IsMasterClient)
        //{
            GameObject obj = PhotonNetwork.Instantiate(Path.Combine("Prefabs", "TinyExplosion_Collision"), transform.position, transform.rotation);
            PhotonNetwork.Destroy(this.gameObject);
        //}
    }
}
