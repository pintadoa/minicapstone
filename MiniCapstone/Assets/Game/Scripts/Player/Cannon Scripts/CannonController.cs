﻿using System.Collections;
using System.Collections.Generic;
using Photon.Realtime;
using Photon.Pun;
using UnityEngine;
using System.IO;
using UnityEngine.UI;

public class CannonController : MonoBehaviour {
    public enum Direction { Right, Left, Forward, Back};

    public KeyCode action;

    //Cannon Variables
    public GameObject cannonBall;
    public Direction shootSide;
    public List <Transform> shotPositions;
    public GameObject explosion;
    public float firePower;
    public float fireRate;
    public bool keyinput = true;

    private float stopWatch;
    private Rigidbody cannonBallRB;
    private bool allowFire;

    private PhotonView pv;

    public Button fireLeftButton;
    GameObject fireLeftButtonGameObject;

    public Button fireRightButton;
    GameObject fireRightButtonGameObject;

    // Use this for initialization
    void Start ()
    {
        allowFire = true;
        //pv = this.GetComponentInParent<PhotonView>();
        pv = this.GetComponent<PhotonView>();

        fireLeftButtonGameObject = GameObject.Find("/Game/UI/Canvas/Fire_Button1/Left_Fire");
        if (fireLeftButtonGameObject.GetComponent<Button>())
        {
            fireLeftButton = fireLeftButtonGameObject.GetComponent<Button>();
            shootSide = Direction.Left;
            fireLeftButton.onClick.AddListener(RPC_SpawnCannon);
        }

        fireRightButtonGameObject = GameObject.Find("/Game/UI/Canvas/Fire_Button2/Right_Fire");
        if (fireRightButtonGameObject.GetComponent<Button>())
        {
            fireRightButton = fireRightButtonGameObject.GetComponent<Button>();
            shootSide = Direction.Right;
            fireRightButton.onClick.AddListener(RPC_SpawnCannon);
        }

        
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (!allowFire)
        {
            stopWatch -= Time.deltaTime;
            if (stopWatch < 0.0f)
            {
                stopWatch = fireRate;
                allowFire = true;
            }
           
        }

        //if (Input.GetKeyDown(action) && keyinput)
        //{
        //    if(pv.IsMine)
        //    {
        //        RPC_SpawnCannon();
        //    }
           
        //}
    }

[PunRPC]
    public void RPC_SpawnCannon()
    {
        if(!pv)
        {
            pv = this.GetComponent<PhotonView>();
            Debug.Log("PV WAS NULL");
        }
        else
        {
            pv.RPC("RPC_Fire", RpcTarget.All);
            //RPC_FireCannon();
        }
    }

[PunRPC]
    public void RPC_Fire()
    {
        if (allowFire)
        {
            allowFire = false;
            if (PhotonNetwork.IsMasterClient)
            {
                foreach (Transform shotPos in shotPositions)
                {
                    GameObject cannonBallCopy = PhotonNetwork.Instantiate(Path.Combine("Prefabs", "CannonBall"), shotPos.position, shotPos.rotation) as GameObject;
                    cannonBallRB = cannonBallCopy.GetComponent<Rigidbody>();


                    float step = firePower * Time.deltaTime;
                    ////Adds force in the desired direction
                    switch (shootSide)
                    {
                        case Direction.Right:
                            cannonBallRB.AddRelativeForce(transform.right * step, ForceMode.Impulse);
                            break;
                        case Direction.Left:
                            cannonBallRB.AddRelativeForce(-transform.right * step, ForceMode.Impulse);
                            break;
                        case Direction.Forward:
                            cannonBallRB.AddRelativeForce(transform.forward * step, ForceMode.Impulse);
                            break;
                        case Direction.Back:
                            cannonBallRB.AddRelativeForce(-transform.forward * step, ForceMode.Impulse);
                            break;
                    }

                    PhotonNetwork.Instantiate(Path.Combine("Prefabs", "TinyExplosion"), shotPos.position, shotPos.rotation);
                }
            }
        }
    }
}
