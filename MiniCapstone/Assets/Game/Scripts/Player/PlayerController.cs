﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{

    public enum Controller
    {
        slider,
        keys
    };

    private PhotonView pv;

    [Range(-1, 1)]
    public int thrust;
    public float acceleration;

    public Controller controller;
    public KeyCode upSpeed;
    public KeyCode downSpeed;

    public Slider playerSlider;
    GameObject sliderGameObject;

    private Rigidbody rb;

    // Use this for initialization
    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody>();
        thrust = 0;
        pv = this.GetComponentInParent<PhotonView>();

        sliderGameObject = GameObject.Find("/Game/UI/Canvas/SliderGroup/Slider");
        if(sliderGameObject.GetComponent<Slider>())
        {
            playerSlider = sliderGameObject.GetComponent<Slider>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        OnKeyPress();
        OnSliderChange();
        rb.AddForce(transform.forward * thrust * acceleration * Time.deltaTime);
    }


    private void Awake()
    {
        // Register players for the Chimera
        PlayerManager.Instance.players.Add(this.gameObject);
    }

    public void OnSliderChange()
    {
        if (!pv.IsMine)
        {
            return;
        }
        if (controller == Controller.slider)
        {
            thrust = (int)playerSlider.value;
        }
    }

    public void OnKeyPress()
    {
        if (!pv.IsMine)
        {
            return;
        }
        if (controller == Controller.keys)
        {
            if (Input.GetKeyDown(upSpeed))
            {
                thrust += 1;
            }

            if (Input.GetKeyDown(downSpeed))
            {
                thrust -= 1;
            }
        }
    }
}