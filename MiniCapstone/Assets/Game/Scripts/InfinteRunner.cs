﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfinteRunner : MonoBehaviour {

    public GameObject[] riverPlanes;
    public static float acceleration;
    public float treadmillSpeed=1.0f;
    public float spawnLength = 10.0f;

    private void Start()
    {
        acceleration = treadmillSpeed;
    }

    void Update()
    {
        // Move the planes depending on the acceleration
        MovePlanes();
        // If the plane is out of view move it back 
        StickPlanes();
    }

    private void MovePlanes()
    {
        for (int i = 0; i < riverPlanes.Length; i++)
        {
            if (riverPlanes[i].transform.position.z < spawnLength)
            {
                riverPlanes[i].transform.Translate(transform.forward * acceleration * Time.deltaTime);
            }
        }
    }

    private void StickPlanes()
    {
        for (int i = 0; i < riverPlanes.Length; i++)
        {
            if (riverPlanes[i].transform.position.z > spawnLength)
            {
                Vector3 startPos;
                if ((i + 1) == riverPlanes.Length)
                {
                    startPos = new Vector3(0.0f, 0.0f, riverPlanes[0].transform.position.z - spawnLength);
                }
                else
                {
                    startPos = new Vector3(0.0f, 0.0f, riverPlanes[i + 1].transform.position.z - spawnLength);
                }
                // Has reached the end reset position
                riverPlanes[i].transform.position = startPos;
            }
        }
    }
}
