﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.iOS;

public class GameStartingLogic : MonoBehaviour
{
    private Camera maincamera;
    public GameObject ARCameraManager;
    public GameObject ARRemoteConnection;
    public GameObject PointCloudP;
    public GameObject EnvironementTweaking;
    public bool isGod;
    // Start is called before the first frame update
    void Start()
    {
        if(Application.platform == RuntimePlatform.IPhonePlayer)
        {
            if(!isGod)
            {
                HideAR();
            }
            else
            {
                EnvironementTweaking.transform.rotation = Quaternion.Euler(15, 15, 0);//new Quaternion(15, 15, 1, 0);
                EnvironementTweaking.transform.localScale = new Vector3(80, 80, 80);
            }
        }
        else
        {
            HideAR();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void HideAR()
    {
        maincamera = Camera.main;
        maincamera.GetComponent<UnityARVideo>().enabled = false;
        maincamera.GetComponent<UnityARCameraNearFar>().enabled = false;
        ARCameraManager.SetActive(false);
        ARRemoteConnection.SetActive(false);
        PointCloudP.SetActive(false);
    }
}
