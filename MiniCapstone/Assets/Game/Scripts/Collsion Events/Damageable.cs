﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class Damageable : MonoBehaviour {

    // there are some events we need to handle
    public UnityEvent OnDeath;
    public UnityEvent OnTakeDamage;

    public float health;
    public float defence;

    private PhotonView pv;

    public Image healthBar;
    public GameObject healthBarGameObject;

    public GameObject GameOverGameObject;

    private void Start()
    {
        pv = this.GetComponentInParent<PhotonView>();


        healthBarGameObject = GameObject.Find("/Game/UI/Canvas/Health/HealthBackground/HealthBar");
        if (healthBarGameObject.GetComponent<Image>())
        {
            healthBar = healthBarGameObject.GetComponent<Image>();
        }

        GameOverGameObject = GameObject.Find("/Game/UI/GameOver");
        GameOverGameObject.SetActive(false);
    }

    private void Update()
    {
        if (pv.IsMine)
        {
            healthBar.fillAmount = (health/100);
        }
    }

    public void ProcessDmg(Damager d, PhotonView pv)
    {
        // apply the damage from the damager who hit me - defence
        health = health + (defence - d.Attack);

        if (health <= 0.0f)
        {
            // handle death cb if we need to
            if (OnDeath != null)
            {
                if(PhotonNetwork.IsMasterClient)
                {
                    //pv = this.gameObject.GetComponent<PhotonView>();
                    //  PhotonNetwork.Destroy(this.gameObject);
                    // OnDeath.Invoke();
                    pv.RPC("NetworkDestroy", RpcTarget.All, pv.ViewID);
                }
                //
            }
            GameOverGameObject.SetActive(true);
        }
        else
        {
            // handle taking damage callback
            if (OnTakeDamage != null)
                OnTakeDamage.Invoke();
        }
    }

    [PunRPC]
    void NetworkDestroy(int viewID)
    {
        Destroy(PhotonView.Find(viewID).gameObject);
    }
}
