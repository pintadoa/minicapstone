﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CollisionVol : MonoBehaviour {

    public string touchType;
    public UnityEvent OnCollisionEnterCB;
    [HideInInspector]
    public GameObject otherObj;

    public void OnCollisionEnter(Collision other)
    {
        // otherObj is a gameobject reference
        if (other.gameObject.GetComponent(touchType) != null)
        {
            // inserted!
            otherObj = other.gameObject;
        }

        if (OnCollisionEnterCB != null)
            OnCollisionEnterCB.Invoke();
    }
}
