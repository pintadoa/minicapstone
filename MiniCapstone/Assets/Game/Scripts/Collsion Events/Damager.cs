﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class Damager : MonoBehaviour {

    public float Attack;
    private GameObject recipient;   // refactor todo
    private PhotonView pv;

    private void Start()
    {

    }
    private void _LateUpdateRecipient()
    {
        // We get Collision Volume of the object
        CollisionVol collisionVol = GetComponent<CollisionVol>();
        if (collisionVol != null)
        {
            recipient = collisionVol.otherObj;
            pv = recipient.GetComponent<PhotonView>();
        }  
    }


    public void DoDamage()
    {
        _LateUpdateRecipient();
        if (recipient != null)
        {
            Damageable dm = recipient.GetComponent<Damageable>();
            if (dm != null)
                dm.ProcessDmg(this,pv);
        }
    }
}
