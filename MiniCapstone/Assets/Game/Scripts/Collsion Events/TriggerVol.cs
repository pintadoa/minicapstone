﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerVol : MonoBehaviour {

    public string touchType;
    public UnityEvent OnTriggerEnterCB;
    [HideInInspector]
    public GameObject otherObj;

    private void OnTriggerEnter(Collider other)
    {
        // otherObj is a gameobject reference
        if (other.gameObject.GetComponent(touchType) != null)
        {
            // inserted!
            otherObj = other.gameObject;
        }

        if (OnTriggerEnterCB != null)
            OnTriggerEnterCB.Invoke();
    }
}
