﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class RoomState : MonoBehaviourPunCallbacks {

    public int numberosOfPlayers;
    public int max;
    public int curreNumOFPLayers;
    public bool openn;
    public int numberOfRooms;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (PhotonNetwork.IsConnected)
        {
            numberosOfPlayers = PhotonNetwork.CountOfPlayers;
            max = PhotonNetwork.CurrentRoom.MaxPlayers;
            curreNumOFPLayers = PhotonNetwork.CurrentRoom.PlayerCount;
            openn = PhotonNetwork.CurrentRoom.IsOpen;
            numberOfRooms = PhotonNetwork.CountOfRooms;
        }
	}
}
