﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonInfo : Button
{
    public string roomName;
    public bool isRoomOpen;
    public int maxRoomSize;
    public int playersInRoom;

    public UnityEvent evnt;


    public override void OnPointerClick(PointerEventData eventData)
    {
        Lobby.instance.JoinRoom(roomName);
    }
}
