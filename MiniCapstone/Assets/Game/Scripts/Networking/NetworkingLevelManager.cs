﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using Photon.Realtime;
using Photon.Pun;
using UnityEngine;

public class NetworkingLevelManager : MonoBehaviourPunCallbacks,IInRoomCallbacks
{
    private PhotonView pv;
    public GameObject playerPrefab;
    public Transform[] spawnPoints;

    public Canvas[] canvas;

    // Start is called before the first frame update
    void Start()
    {
        pv = this.GetComponent<PhotonView>();
        if(PhotonNetwork.IsMasterClient)
        {
            RPC_CreatePlayer();
        }
    }

    [PunRPC]
    private void RPC_CreatePlayer()
    {
        pv.RPC("RPC_SpawnPlayers", RpcTarget.All);
    }

    [PunRPC]
    private void RPC_SpawnPlayers()
    {
        GameStartingLogic gameLogic = this.GetComponent<GameStartingLogic>();
        if (PhotonNetwork.IsMasterClient)
        {
            canvas[0].enabled = false;
            canvas[1].enabled = true;
            gameLogic.isGod = true;
        }
        else
        {
            canvas[0].enabled = true;
            canvas[1].enabled = false;
            gameLogic.isGod = false;
            GameObject obj = PhotonNetwork.Instantiate(this.playerPrefab.name, spawnPoints[Room.roomInstance.number-2].transform.position, Quaternion.identity);

        }
    }

    public override void OnEnable()
    {
        base.OnEnable();
        PhotonNetwork.AddCallbackTarget(this);
    }

    public override void OnDisable()
    {
        base.OnDisable();
        PhotonNetwork.RemoveCallbackTarget(this);
    }

}
