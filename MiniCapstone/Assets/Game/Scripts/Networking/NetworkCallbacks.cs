﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class NetworkCallbacks : MonoBehaviourPunCallbacks
{
    public GameObject contentArea;
    public GameObject button;

    void Awake()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
    }

    void Start()
    {
        Connect();
    }

    private void Connect()
    {
        // we check if we are connected or not, we join if we are , else we initiate the connection to the server.
        if (PhotonNetwork.IsConnected)
        {
            // #Critical we need at this point to attempt joining a Random Room. If it fails, we'll get notified in OnJoinRandomFailed() and we'll create one.
            PhotonNetwork.JoinRandomRoom();
        }
        else
        {
            // #Critical, we must first and foremost connect to Photon Online Server.
            //PhotonNetwork.GameVersion = "2.5.0";
            PhotonNetwork.ConnectUsingSettings();
        }
    }

    public override void OnConnectedToMaster()
    {
        Debug.Log("Connected to master");
        PhotonNetwork.JoinLobby();
    }


    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.Log(message);

        Lobby lobby = Lobby.instance;

        //lobby.CreateRoom();
        //     lobby.JoinRoom();
    }

    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        Debug.Log(message);
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        foreach (var roomInfo in roomList)
        {
            ButtonInfo btn = Instantiate(button, contentArea.transform).GetComponent<ButtonInfo>();
            btn.roomName = roomInfo.Name;
        }
    }

    public override void OnJoinedLobby()
    {
        Debug.Log("Connected to Lobby");
    }
}
