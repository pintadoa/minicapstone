﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiPlayerSettings : MonoBehaviour
{
    public static MultiPlayerSettings multiPlayerSettingsInstance;

    public bool delayStart;
    public int maxPlayers;
    public int menuScene;
    public int gameScene;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void Awake()
    {
        if(!multiPlayerSettingsInstance)
        {
            multiPlayerSettingsInstance = this;
        }
        else
        {
            if(multiPlayerSettingsInstance != this)
            {
                Destroy(this.gameObject);
                multiPlayerSettingsInstance = this;
            }
        }
        DontDestroyOnLoad(this.gameObject);
    }
}
