﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;

public class Lobby : MonoBehaviour
{
    public static Lobby instance;

    void Awake()
    {
        if (instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }
    }

    public void JoinRoom()
    { 

    }

    public void JoinRoom(string roomName)
    {
        PhotonNetwork.JoinRoom(roomName);
    }

    public void JoinRandomRoom()
    {
        PhotonNetwork.JoinRandomRoom();
    }

    public void CreateAndJoin(InputField field)
    {
        string roomName = field.text;
        CreateRoom(roomName);
    }

    private void CreateRoom(string roomName)
    {
        RoomOptions roomOps = new RoomOptions()
        {
            IsVisible = true,
            IsOpen = true,
            MaxPlayers = (byte)MultiPlayerSettings.multiPlayerSettingsInstance.maxPlayers
        };

        PhotonNetwork.CreateRoom(roomName, roomOps);
    }

    public void ExitLobby(Collision collision)
    {
        PhotonNetwork.LeaveRoom();
    } 

    private bool ExistingRoom(string name)
    {
        return true;
    } 
}

