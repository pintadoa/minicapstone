﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Room : MonoBehaviourPunCallbacks, IInRoomCallbacks
{
    [Tooltip("The Ui Text to inform the user about the connection progress")]
    [SerializeField]
    private Text feedbackText;

    public static Room roomInstance;

    public int currentScene;

    public GameObject playerPrefab;

    public bool isGameLoaded;
    private PhotonView pv;

    public int numberosOfPlayers;
    public int max;
    public int curreNumOFPLayers;
    public bool openn;
    public int numberOfRooms;

    //Player Info
    Player[] photonPlayers;
    public int playersInRoom;
    public int number;//Player number in Room
    public int playerInGame;

    //DelayedStart 

    // Use this for initialization
    void Start()
    {
        pv = this.GetComponent<PhotonView>();
        isGameLoaded = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (playersInRoom == MultiPlayerSettings.multiPlayerSettingsInstance.maxPlayers && !isGameLoaded)
        {
            isGameLoaded = true;
            if (PhotonNetwork.IsMasterClient)
            {
                PhotonNetwork.CurrentRoom.IsOpen = false;

                PhotonNetwork.LoadLevel(MultiPlayerSettings.multiPlayerSettingsInstance.gameScene);
            }
        }
    }

    private void Awake()
    {
        if (!roomInstance)
        {
            roomInstance = this;
        }
        else
        {
            if (roomInstance != this)
            {
                Destroy(this.gameObject);
            }
        }
        DontDestroyOnLoad(this.gameObject);
    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        LogFeedback("<Color=Green>OnJoinedRoom</Color> with " + PhotonNetwork.CurrentRoom.PlayerCount + " Player(s)");
        Debug.Log("PUN Basics Tutorial/Launcher: OnJoinedRoom() called by PUN. Now this client is in a room.\nFrom here on, your game would be running.");

        photonPlayers = PhotonNetwork.PlayerList;
        playersInRoom = photonPlayers.Length;
        number = playersInRoom;
        PhotonNetwork.NickName = number.ToString();
    }

    void LogFeedback(string message)
    {
        // we do not assume there is a feedbackText defined.
        if (feedbackText == null)
        {
            return;
        }

        // add new messages as a new line and at the bottom of the log.
        feedbackText.text += System.Environment.NewLine + message;
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);

        photonPlayers = PhotonNetwork.PlayerList;
        playersInRoom++;
    }

    void OnSceneFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        currentScene = scene.buildIndex;
        if (currentScene == MultiPlayerSettings.multiPlayerSettingsInstance.gameScene)
        {
            isGameLoaded = true;

            if (!PhotonNetwork.IsMasterClient)
            {
             //  RPC_CreatePlayer();
            }
        }
    }

        //[PunRPC]
        //private void RPC_CreatePlayer()
        //{
        //  pv.RPC("RPC_SpawnPlayers", RpcTarget.All);
        //}
    
        //[PunRPC]
        //private void RPC_SpawnPlayers()
        //{
        //    GameObject obj = PhotonNetwork.Instantiate(this.playerPrefab.name, spawnPoints[number - 1], Quaternion.identity);
        //}
}
