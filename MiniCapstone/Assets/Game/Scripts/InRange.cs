﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InRange : MonoBehaviour {

    public string touchType = "PlayerController";
    public string boolRange = "IsInRange";
    private Animator sSAnimator;

    [HideInInspector]
    public GameObject target;

    private void Awake()
    {
        sSAnimator = gameObject.GetComponentInParent<Animator>();
    }

    private void isTouchTypeInRange(Collider other)
    {
        if (other.GetComponent(touchType))
        {
            target = other.gameObject;
            sSAnimator.SetBool(boolRange, true);
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        isTouchTypeInRange(other);
    }

    public void OnTriggerStay(Collider other)
    {
        isTouchTypeInRange(other);
    }

    private void OnTriggerExit(Collider other)
    {
        sSAnimator.SetBool(boolRange, false);
    }
}
