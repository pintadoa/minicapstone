﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeekSteeringBehaviour : SteeringBehaviourBase
{
    public ChimeraLogic chimeraLogic;

    private void Start()
    {
    }

    public override Vector3 calculateForce()
    {
        if(chimeraLogic.target != null)
        {
            Target = chimeraLogic.target;
            Vector3 desiredVelocity = (Target - gameObject.transform.position).normalized;
            desiredVelocity = desiredVelocity * steeringComponent.MaxSpeed;
            return desiredVelocity - steeringComponent.Velocity;
        }
        return Vector3.zero;
    }
}
