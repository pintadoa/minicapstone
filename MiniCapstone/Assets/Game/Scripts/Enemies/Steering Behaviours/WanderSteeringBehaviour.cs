﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WanderSteeringBehaviour : SteeringBehaviourBase
{
    public float WanderDistance = 2.0f;
    public float WanderRadius = 1.0f;
    public float WanderJitter = 20.0f;

    public float range;
    public Transform centerArea;

    private Vector3 WanderTarget;

    private void Start()
    {
        float theta = (float)Random.value * Mathf.PI * 2;
        WanderTarget = new Vector3(WanderRadius * (float)Mathf.Sin(theta), 0.0f, WanderRadius * (float)Mathf.Cos(theta));
    }

    public override Vector3 calculateForce()
    {
        if (centerArea != null)
        {
            Target = centerArea.position;

            float distance = (gameObject.transform.position - Target).magnitude;
            if (distance < range)
            {
                float jitterThisTimeSlice = WanderJitter * Time.deltaTime;

                WanderTarget = WanderTarget + new Vector3(Random.Range(-1.0f, 1.0f) * jitterThisTimeSlice, 0.0f,
                                                            Random.Range(-1.0f, 1.0f) * jitterThisTimeSlice);
                WanderTarget.Normalize();
                WanderTarget = WanderTarget * WanderRadius;

                // Move point the desired distance
                Target = WanderTarget + new Vector3(0, 0, WanderDistance);
                // Transform to world coordiantes
                Target = gameObject.transform.rotation * Target + gameObject.transform.position;

                Vector3 wanderForce = Target - gameObject.transform.position;
                wanderForce.Normalize();
                wanderForce = wanderForce * steeringComponent.MaxSpeed;

                return wanderForce;
            }
            else
            {
                Vector3 desiredVelocity = (Target - gameObject.transform.position).normalized;
                desiredVelocity = desiredVelocity * steeringComponent.MaxSpeed;
                return desiredVelocity - steeringComponent.Velocity;
            }
        }

        return Vector3.zero;
        //float jitterThisTimeSlice = WanderJitter * Time.deltaTime;

        //WanderTarget = WanderTarget + new Vector3(Random.Range(-1.0f, 1.0f) * jitterThisTimeSlice, 0.0f, 
        //                                            Random.Range(-1.0f, 1.0f) * jitterThisTimeSlice);
        //WanderTarget.Normalize();
        //WanderTarget = WanderTarget * WanderRadius;

        //// Move point the desired distance
        //Target = WanderTarget + new Vector3(0, 0, WanderDistance);
        //// Transform to world coordiantes
        //Target = gameObject.transform.rotation * Target + gameObject.transform.position;

        //Vector3 wanderForce = Target - gameObject.transform.position;
        //wanderForce.Normalize();
        //wanderForce = wanderForce * steeringComponent.MaxSpeed;

        //return wanderForce;
    }

    private void OnDrawGizmos()
    {
        //if (debugDraw)
        //{
        //    if(steeringComponent!=null && gameObject!=null)
        //    {

        //        DebugExtension.DrawArrow(gameObject.transform.position, steeringComponent.Velocity.normalized, Color.red);

        //        Vector2 circleCenter = gameObject.transform.rotation * new Vector3(0, 0, WanderDistance) + gameObject.transform.position;
        //        DebugExtension.DrawCircle(circleCenter, new Vector3(0, 1, 0), Color.white, WanderRadius);
        //        Debug.DrawLine(gameObject.transform.position, Target, Color.blue);
        //    }
            
        //}
    }
}
