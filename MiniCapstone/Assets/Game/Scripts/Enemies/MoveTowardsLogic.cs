﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTowardsLogic : MonoBehaviour {

    public GameObject model;
    public Transform attackPosition;
    public Transform startPosition;

    public bool MoveTowardsTarget(Transform start, Transform goal, float speed, float duration, float length)
    {
        float distCovered = duration * speed;
        float fracJourney = distCovered / length;
        model.transform.position = Vector3.Lerp(start.position, goal.position, fracJourney);

        if (Vector3.Distance(model.transform.position,goal.position) < 0.1)
        {
            return true;
        }

        return false;
    }
}
