﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointMovement : MonoBehaviour
{
    public float speed;
    public bool loop = false;
    public Transform WaypointManager; // GameObject with all the waypoints inside it

    private Transform targetWaypoint; // Current target
    private int index = 0;
    private bool isNavigateFinished = false;

    void Start()
    {
        index = 0;
        targetWaypoint = WaypointManager.transform.GetChild(0);
    }

    private void Update()
    {
        FolllowWaypoints();
    }

    private void FolllowWaypoints()
    {
        if (!isNavigateFinished)
        {
            // Move towards the targetWaypoint
            this.transform.position = Vector3.MoveTowards(this.transform.position, targetWaypoint.position, speed * Time.deltaTime);

            if (Vector3.Distance(transform.position, targetWaypoint.position) < 0.1)
            {
                if (index == 0 && loop)
                {
                    Flip();
                }

                index++;

                // Change to original camera
                if (index % WaypointManager.childCount != 0)
                {
                    // Get new target waypoint
                    targetWaypoint = WaypointManager.GetChild(index);
                }
                else
                {
                    if (loop)
                    {
                        index = 0;
                        targetWaypoint = WaypointManager.transform.GetChild(index);
                        Flip();
                    }
                    else
                    {
                        isNavigateFinished = true;
                    }
                }
            }
        }
    }

    private void Flip()
    {
        gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x * (-1), gameObject.transform.localScale.y, this.gameObject.transform.localScale.z);
    }

}
