﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationLogic : MonoBehaviour {

    public GameObject ObjetToRotate;
    public float speed;
    public float epsilon;      // for deciding when to stop

    private float dpAngle; 
    private float dpLeftOrRight;
    private Vector3 vT2T;

    public bool LookTowardsTarget(GameObject target)
    {
        vT2T = target.transform.position - ObjetToRotate.transform.position;
        vT2T.Normalize();

        dpAngle = Vector3.Dot(-ObjetToRotate.transform.right, vT2T);
        dpLeftOrRight = Vector3.Dot(ObjetToRotate.transform.right, vT2T);    // +ve, turn right, -ve turn left

        // dont rotate if we are facing within epsilon
        if (Mathf.Abs(dpAngle) < epsilon)
        {
            return true;
        }
        else
        {
            if (dpLeftOrRight > 0.0f)
                ObjetToRotate.transform.Rotate(Vector3.up, speed * Time.deltaTime);
            else
                ObjetToRotate.transform.Rotate(Vector3.up, -speed * Time.deltaTime);
        }
        return false;
    }
}
