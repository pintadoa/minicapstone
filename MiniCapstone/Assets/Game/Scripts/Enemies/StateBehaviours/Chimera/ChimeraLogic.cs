﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChimeraLogic : MonoBehaviour {

    public GameObject model;
    public Transform startPosition;

    public Vector3 target;

    private SteeringBehaviour steeringBehaviour;
    private WanderSteeringBehaviour wanderSteeringBehaviour;
    private SeekSteeringBehaviour seekSteeringBehaviour;

    private void Awake()
    {
        steeringBehaviour = model.GetComponent<SteeringBehaviour>();
        if (steeringBehaviour == null) 
        {
            Debug.Log("The Chimera is missing the Steering Behaviour script");
        }

        seekSteeringBehaviour = model.GetComponent<SeekSteeringBehaviour>();
        if (seekSteeringBehaviour == null)
        {
            Debug.Log("The Chimera is missing the SeekSteering Behaviour script");
        }

        wanderSteeringBehaviour = model.GetComponent<WanderSteeringBehaviour>();
        if (seekSteeringBehaviour == null)
        {
            Debug.Log("The Chimera is missing the WanderSteering Behaviour script");
        }
    }

    public void enableSeekSteeringBehaviour()
    {
        steeringBehaviour.enabled = true;
        seekSteeringBehaviour.enabled = true;
    }

    public void disableSeekSteeringBehaviour()
    {
        steeringBehaviour.enabled = false;
        seekSteeringBehaviour.enabled = false;
    }

    public void enableWanderSteeringBehaviour()
    {
        steeringBehaviour.enabled = true;
        wanderSteeringBehaviour.enabled = true;
    }

    public void disableWanderSteeringBehaviour()
    {
        steeringBehaviour.enabled = false;
        wanderSteeringBehaviour.enabled = false;
    }


}
