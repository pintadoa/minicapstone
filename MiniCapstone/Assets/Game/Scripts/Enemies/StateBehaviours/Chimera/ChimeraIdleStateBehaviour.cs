﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChimeraIdleStateBehaviour : StateMachineBehaviour
{
    public float chimeraRange = 100.0f;
    public string inRangeTrigger = "InRange";

    private ChimeraLogic chimeraLogic;


    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        chimeraLogic = animator.GetComponent<ChimeraLogic>();
        if (chimeraLogic == null)
        {
            Debug.LogWarning(animator.gameObject.name + " is missing the ChimeraLogic script");
            return;
        }
        chimeraLogic.disableSeekSteeringBehaviour();
        chimeraLogic.enableWanderSteeringBehaviour();
        chimeraLogic.target = new Vector3(100.0f, 100.0f, 100.0f);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        foreach(GameObject player in PlayerManager.Instance.players)
        {
            if(Vector3.Distance(player.transform.position, chimeraLogic.model.transform.position) < chimeraRange && chimeraLogic.target == new Vector3(100.0f, 100.0f, 100.0f))
            {
                chimeraLogic.target = player.transform.position;
                chimeraLogic.disableWanderSteeringBehaviour();
                animator.SetTrigger(inRangeTrigger);    
            }
        }
    }
}
