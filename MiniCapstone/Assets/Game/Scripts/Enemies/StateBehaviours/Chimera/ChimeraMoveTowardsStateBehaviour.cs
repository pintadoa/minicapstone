﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChimeraMoveTowardsStateBehaviour : StateMachineBehaviour
{
    public string inPosition = "triggerAtteck";

    private ChimeraLogic chimeraLogic;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

        chimeraLogic = animator.GetComponent<ChimeraLogic>();
        if (chimeraLogic == null)
        {
            Debug.LogWarning(animator.gameObject.name + " is missing the MoveTowardsLogic script");
            return;
        }

        chimeraLogic.enableSeekSteeringBehaviour();
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (Vector3.Distance(chimeraLogic.model.transform.position, chimeraLogic.target) < 0.2f) 
        {
            chimeraLogic.disableSeekSteeringBehaviour();
            animator.SetTrigger(inPosition);
        }
    }
}
