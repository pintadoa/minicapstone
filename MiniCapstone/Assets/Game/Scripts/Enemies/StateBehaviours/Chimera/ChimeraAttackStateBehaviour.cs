﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChimeraAttackStateBehaviour : StateMachineBehaviour
{
    public string attackAnimaton = "Attack";
    public string returnTrigger = "returnTrigger";

    private ChimeraLogic chimeraLogic;
    private Animator chimeraAnimator;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        chimeraLogic = animator.GetComponent<ChimeraLogic>();
        if (chimeraLogic == null)
        {
            Debug.LogWarning(animator.gameObject.name + " is missing the chimeraLogic script");
            return;
        }
        else
        {
            chimeraAnimator = chimeraLogic.model.GetComponent<Animator>();
            if (chimeraAnimator == null)
            {
                Debug.LogWarning(chimeraLogic.model.name + " is missing the Animator");
                return;
            }
        }
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (chimeraAnimator != null)
        {
            if (chimeraAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && !chimeraAnimator.IsInTransition(0))
            {
                chimeraAnimator.SetTrigger(attackAnimaton);
            }
            else
            {
                animator.SetTrigger(returnTrigger);
            }
        }
    }
}
