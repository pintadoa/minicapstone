﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChimeraReturnToPositionStateBehaviour : StateMachineBehaviour
{
    public string triggerIdle = "triggerIdle";

    private ChimeraLogic chimeraLogic;
    private Animator chimeraAnimator;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        chimeraLogic = animator.GetComponent<ChimeraLogic>();
        if (chimeraLogic == null)
        {
            Debug.LogWarning(animator.gameObject.name + " is missing the chimeraLogic script");
            return;
        }
        chimeraLogic.target = new Vector3(Random.RandomRange(-5.0f, 5.0f), 2.770004f, Random.RandomRange(-5.0f, 5.0f));
        chimeraLogic.enableSeekSteeringBehaviour();
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (Vector3.Distance(chimeraLogic.model.transform.position, chimeraLogic.target) < 0.5f)
        {
            chimeraLogic.disableSeekSteeringBehaviour();
            animator.SetTrigger(triggerIdle);
        }
    }
}
