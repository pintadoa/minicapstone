﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SSFaceTargetStateBehaviour : StateMachineBehaviour {

    public string triggerAttack = "Attack";

    private RotationLogic rotationObject;
    private InRange inRange;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        rotationObject = animator.GetComponent<RotationLogic>();
        if (rotationObject == null)
        {
            Debug.LogWarning(animator.gameObject.name + " is missing the RotationLogic script");
            return;
        }
        inRange = animator.GetComponentInChildren<InRange>();
        if (inRange == null)
        {
            Debug.LogWarning(animator.gameObject.name + " is missing the InRange script");
            return;
        }
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (inRange.target != null)
        {
            bool isFacing = rotationObject.LookTowardsTarget(inRange.target);
            if (isFacing)
            {
                animator.SetTrigger(triggerAttack);
            }
        }
    }
}
