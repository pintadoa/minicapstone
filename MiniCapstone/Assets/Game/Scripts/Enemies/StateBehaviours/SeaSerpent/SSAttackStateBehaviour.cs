﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class SSAttackStateBehaviour : StateMachineBehaviour {


    private CannonController cannonController;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        cannonController = animator.GetComponent<CannonController>();
        if (cannonController == null)
        {
            Debug.LogWarning(animator.gameObject.name + " is missing the cannonController script");
            return;
        }
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        cannonController.RPC_SpawnCannon();
    }

}
