﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhaleAttackStateBehaviour : StateMachineBehaviour {
    public string attackAnimaton = "BiteTrigger";
    public string returnTrigger = "ReturnTrigger";

    private MoveTowardsLogic whaleLogic;
    private Animator whaleAnimator;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        whaleLogic = animator.GetComponent<MoveTowardsLogic>();
        if (whaleLogic == null)
        {
            Debug.LogWarning(animator.gameObject.name + " is missing the MovementLogic script");
            return;
        }
        else
        {
            whaleAnimator = whaleLogic.model.GetComponent<Animator>();
            if (whaleAnimator == null)
            {
                Debug.LogWarning(whaleLogic.model.name + " is missing the Animator script");
                return;
            }
        }
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if(whaleAnimator!=null)
        {
            if (whaleAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && !whaleAnimator.IsInTransition(0))
            {
               whaleAnimator.SetTrigger(attackAnimaton);
            }
            else
            {
                animator.SetTrigger(returnTrigger);
            }
        } 
    }
}
