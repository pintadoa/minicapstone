﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhaleIdleStateBehaviour : StateMachineBehaviour {
    public string moveToTrigger= "MoveToTrigger";
    public float duration = 3.0F;

    private float elapsedTime;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        elapsedTime = 0.0f;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        elapsedTime += Time.deltaTime;
        if (elapsedTime > duration)
        {
            animator.SetTrigger(moveToTrigger);
        }
    }
}
