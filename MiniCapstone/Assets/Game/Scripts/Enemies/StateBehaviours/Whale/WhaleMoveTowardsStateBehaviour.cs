﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhaleMoveTowardsStateBehaviour : StateMachineBehaviour {

    public string inPosition = "InPosition";
    public float speed = 1.0F;
    public bool isReverse = false;

    private MoveTowardsLogic whaleLogic;
    private float elapsedTime;
    private float journeyLength;
    private bool reachedGoal;

    private Transform startPosition;
    private Transform endPosition;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        whaleLogic = animator.GetComponent<MoveTowardsLogic>();
        if (whaleLogic == null)
        {
            Debug.LogWarning(animator.gameObject.name + " is missing the CrabLogic script");
            return;
        }
        elapsedTime = 0.0f;
        reachedGoal = false;
        journeyLength = Vector3.Distance(whaleLogic.startPosition.position, whaleLogic.attackPosition.position);

        if(!isReverse)
        {
            startPosition = whaleLogic.startPosition;
            endPosition = whaleLogic.attackPosition;
        }
        else
        {
            startPosition = whaleLogic.attackPosition;
            endPosition = whaleLogic.startPosition;
        }
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        elapsedTime += Time.deltaTime;
        if (!reachedGoal)
        {
            if (whaleLogic.MoveTowardsTarget(startPosition, endPosition, speed, elapsedTime, journeyLength))
            {
                // Target is reached
                elapsedTime = 0.0f;
                reachedGoal = true;
            }
        }
        else
        {
            animator.SetBool(inPosition, true);
            reachedGoal = false;
        }

    }
}
