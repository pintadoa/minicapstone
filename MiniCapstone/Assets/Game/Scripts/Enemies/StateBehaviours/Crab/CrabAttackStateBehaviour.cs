﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrabAttackStateBehaviour : StateMachineBehaviour {

    public string isCoolingBool = "IsCooling";
    public float speed = 1.0F;

    private MoveTowardsLogic crabLogic;
    private float elapsedTime;
    private float journeyLength;
    private bool reachedGoal;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        crabLogic = animator.GetComponent<MoveTowardsLogic>();
        if (crabLogic == null)
        {
            Debug.LogWarning(animator.gameObject.name + " is missing the CrabLogic script");
            return;
        }
        elapsedTime = 0.0f;
        reachedGoal = false;
        journeyLength = Vector3.Distance(crabLogic.startPosition.position, crabLogic.attackPosition.position);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        elapsedTime += Time.deltaTime;
        if (!reachedGoal)
        {
            if (crabLogic.MoveTowardsTarget(crabLogic.startPosition, crabLogic.attackPosition, speed, elapsedTime, journeyLength))
            {
                // Target is reached
                elapsedTime = 0.0f;
                reachedGoal = true;
            }
        }
        else
        {
            animator.SetBool(isCoolingBool, true);
            reachedGoal = false;
        }
        
    }
}
