﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnArea : MonoBehaviour {

    public float lifeTime;
    public GameObject spawnEvent;

    // Update is called once per frame
    void Update()
    {
        StatusCheck();
    }

    void StatusCheck()
    {
        lifeTime -= Time.deltaTime;

        if (lifeTime < 0.0f)
        {
            Instantiate(spawnEvent, transform.position, transform.rotation);
            Destroy(this.gameObject);
        }
    }
}
