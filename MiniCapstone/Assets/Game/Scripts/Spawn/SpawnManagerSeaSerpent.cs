﻿using System.Collections;
using System.Collections.Generic;
using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.XR.iOS;

public class SpawnManagerSeaSerpent : MonoBehaviourPunCallbacks, IInRoomCallbacks
{
    public Camera ARCamera;
    public GameObject spawn;
    public Transform p1Lane;
    public Transform p2Lane;
    public GameObject enemyObject;
    //public KeyCode key;
    public float range = 4.0f;
    public float coolDown;

    private float stopWatch;
    private bool allowSpawn;

    private PhotonView pv;
    private readonly byte CustomManualInstantiationEventCode = 3;
    // Use this for initialization
    void Start()
    {
        allowSpawn = true;
        pv = this.GetComponent<PhotonView>();
    }

    public void SpawnHitObject()
    {
        Debug.Log("Spawn hit object triggered");
        if (Application.platform != RuntimePlatform.IPhonePlayer)
        {
            Spawn();
            return;
        }
        Debug.Log("is iPhone");
        ARPoint point = new ARPoint
        {
            x = 0.5f, //do a hit test at the center of the screen
            y = 0.5f
        };

        // prioritize result types
        ARHitTestResultType[] resultTypes = {
            //ARHitTestResultType.ARHitTestResultTypeExistingPlaneUsingExtent, // if you want to use bounded planes
            //ARHitTestResultType.ARHitTestResultTypeExistingPlane, // if you want to use infinite planes 
            ARHitTestResultType.ARHitTestResultTypeFeaturePoint // if you want to hit test on feature points
        };

        foreach (ARHitTestResultType resultType in resultTypes)
        {
            if (HitTestWithResultType(point, resultType))
            {
                return;
            }
        }
    }

    bool HitTestWithResultType(ARPoint point, ARHitTestResultType resultTypes)
    {
        Debug.Log("HitTestWithResultType triggered");
        List<ARHitTestResult> hitResults = UnityARSessionNativeInterface.GetARSessionNativeInterface().HitTest(point, resultTypes);
        if (hitResults.Count > 0)
        {
            foreach (var hitResult in hitResults)
            {
                Debug.Log("foreach triggered");
                Vector3 pos = UnityARMatrixOps.GetPosition(hitResult.worldTransform);
                Quaternion rotation = UnityARMatrixOps.GetRotation(hitResult.worldTransform);
                //spawnedObjects.Add(Instantiate(hitPrefab, pos, rotation));
                Spawn();
                return true;
            }
        }
        return false;
    }

    // Update is called once per frame
    void Update()
    {
        if(!allowSpawn)
        {
            stopWatch -= Time.deltaTime;
            if (stopWatch < 0.0f)
            {
                allowSpawn = true;
            }
        }
    }

    public override void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    public override void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }

    public void Spawn()
    {
        if (PhotonNetwork.IsMasterClient && allowSpawn)
        {
            allowSpawn = false;
            stopWatch = coolDown;
            spawn.transform.localScale = new Vector3(-1f, 1f, 1f);
            Vector3 position = new Vector3(p1Lane.position.x, 0, Random.Range(-range, range));
            if(!pv)
            {
                pv = this.GetComponent<PhotonView>();
            }

                object[] data = new object[]
                {
                  position, Quaternion.identity, pv.ViewID
                };

                RaiseEventOptions raiseEventOptions = new RaiseEventOptions
                {
                    Receivers = ReceiverGroup.All,
                    CachingOption = EventCaching.AddToRoomCache
                };

                SendOptions sendOptions = new SendOptions
                {
                    Reliability = true
                };

                PhotonNetwork.RaiseEvent(CustomManualInstantiationEventCode, data, raiseEventOptions, sendOptions);

            position = new Vector3(p2Lane.position.x, 0, Random.Range(-range, range));
            spawn.transform.localScale = new Vector3(1f, 1f, 1f);

            object[] data2 = new object[]
            {
                  position, Quaternion.identity, pv.ViewID
            };

            RaiseEventOptions raiseEventOptions2 = new RaiseEventOptions
            {
                Receivers = ReceiverGroup.All,
                CachingOption = EventCaching.AddToRoomCache
            };

            SendOptions sendOptions2 = new SendOptions
            {
                Reliability = true
            };

            PhotonNetwork.RaiseEvent(CustomManualInstantiationEventCode, data2, raiseEventOptions2, sendOptions2);
        }

    }

    public void OnEvent(EventData photonEvent)
    {
        if (photonEvent.Code == CustomManualInstantiationEventCode)
        {
            object[] data = (object[])photonEvent.CustomData;

            GameObject player = (GameObject)Instantiate(enemyObject, (Vector3)data[0], (Quaternion)data[1]);
            pv.ViewID = (int)data[2];
        }
    }

}
